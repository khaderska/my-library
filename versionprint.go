package mylibrary

import "runtime"

func GetVersion() string {
	return runtime.Version()
}

